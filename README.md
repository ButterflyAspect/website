ButterflyAspect Homepage
============
![Framework](https://img.shields.io/badge/Framework-hugo-pink) ![Lizenz](https://img.shields.io/badge/Lizenz-CC--BY--NC--SA--4.0-orange)

# Allgemein

Dies ist das Repository der Homepage meines d10 Pen and Paper Regelwerks welche mit Hugo erstellt wurde.

## Bilder

Die Hintergrundbilder sind von folgenden Künstlern erstellt worden:
* https://www.artstation.com/aaronflorento
* https://www.artstation.com/jobpmenting

## Aufbau/Branches

* **master** ist der stand welcher über den pages Branch veröffentlicht wird, siehe dazu auch [pre-push git-branch hugo](https://codeberg.org/Tealk/Git-Hooks_examples/src/branch/master/pre-push%20git-branch%20hugo)
* **develope** ist der Entwicklungsbranch
* **pages** ist der Branch der die Webseite ausliefert