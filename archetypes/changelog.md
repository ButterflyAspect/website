---
title: {{ .Date | time.Format "2006-01-02" }}
date: {{ .Date }}
tags:
  - Grundregelwerk
  - Magieregelwerk
  - Konstruktionsregelwerk
  - Stuntbeispiele
  - Rassenbeispiele
  - Preisliste
categories:
  - Changelog
author: Tealk
summary: Ein Regelwerk update wurde veröffentlicht und folgendes wurde geändert
showToc: false
hidemeta: true
disableHLJS: true
ShowReadingTime: false
ShowShareButtons: true
draft: false
---

## Grundregelwerk

### Add

- foo

### Changed

- foo

### Remove

- foo

## Magieregelwerk

### Add

- foo

### Changed

- foo

### Remove

- foo

## Konstruktionsregelwerk

### Add

- foo

### Changed

- foo

### Remove

- foo

## Stuntbeispiele

### Add

- foo

### Changed

- foo

### Remove

- foo

## Rassenbeispiele

### Add

- foo

### Changed

- foo

### Remove

- foo

## Preisliste

### Add

- foo

### Changed

- foo

### Remove

- foo
