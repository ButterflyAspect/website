## Hintergrund

Fate gefiel mir schon immer sehr wegen seine Offenheit, daher habe ich es sehr gerne gespielt. Das Würfelsystem hingegen bring eher wenig spielspannung mit sich, was mich veranlasst hat dieses Regelwerk zu erstellen. Also habe ich damit angefangen Fate ein neues Würfelsystem zu geben, was zur folge hatte das ich auch alle anderen Elemente anpassen musste. Ziehl ist es genug Funktionen und Anleitungen an die Hand zu geben um es leicht zu Spielen/Leiten aber nicht so viele das es einschenkt oder einen erschlägt.

## Das System

Butterfly Aspekt hat die Aspekte, die Stunts und die Fate Punkte aus Fate übernommen. Dazu gibt es aber ein neues D10 Würfelsystem. Durch die einfachen Regeln sowie die Beispiele ist es auch für Einsteiger geeignet. Die Charaktere können komplett frei erstellt werden, denn es gibt kein Klassen System. Auch die Magie ist genauso offen gehalten, so das man sich mit ein paar Punkten selbst Zauber ausdenken kann. Das System kann mit allen erdenklichen Settings gespielt werden, egal ob schleichender Halunke in einem Mittelalterdorf oder ein vollvercyberter Straßensamurei im Jahre 2200.
Hier eine kurze Übersicht über die vorhandenen Werke:

## Elemente

- **Grundregelwerk:** Beinhaltet alles um eine einfache Runde zu starten
- **Magieregelwerk:** Beschreibt alles rund um Magie und Magische Objekte
- **Konstruktionsregelwerk:** Konstruktion als auch Alchemie
- **Zusätze:** Hilfen für Spieler und Spielleiter
  - **Charakterbogen:** Eine PDF um eure Werte festzuhalten
  - **Stuntbeispiele:** Beinhaltet Ideen für Stunts
  - **Roll20:** Für Roll20 gibt es einen fertigen Charakterbogen

## Die Spielmechanik

Die Spielmechanik ist einfach gehalten. Man würfelt Proben nur wenn es wirklich in die Situation passt, um den Spielfluss nicht zu stören. Das heißt, Proben werden gewürfelt, wenn es fraglich ist, ob man irgendetwas schafft oder ob man scheitert. Hier noch 3 Fragen die einem dabei helfen können:

- Ist Probe für das weitere Spiel wirklich von Belang?
- Ergibt sich aus der Probe eine interessante Wendung?
- Befindet sich der Charakter in einer Stresssituation?

Wenn all das nicht der Fall ist, kann man auf die Probe verzichten. Niemand wird interessieren, ob man aufstehen oder gefahrlos den weg entlanggehen kann.
Es wird sowohl auf Schwierigkeit als auch gegeneinander gewürfelt, letzteres eigentlich nur in Konfliktsituationen.

## Die Charaktererstellung

Die Erstellung eines neuen Charakters erfolgt nach einem Fertigkeit- und Attributsystem. Des weiteren gibt es Aspekte und Stunts welche frei vergeben werden können.
