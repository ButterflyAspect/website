---
title: 2020-03-15
date: 2020-03-15
tags:
  - Grundregelwerk
  - Magieregelwerk
categories:
  - Changelog
author: Tealk
summary: Ein Regelwerk update wurde veröffentlicht und folgendes wurde geändert
showToc: false
hidemeta: true
disableHLJS: true
ShowReadingTime: false
ShowShareButtons: true
draft: false
---

## Grundregelwerk

### Add

- Verteidigungs Geschickbonus cap eingefügt

### Changed

- Verteidigungsberechnung

## Magieregelwerk

### Changed

- Schaden beim Beispiel Feuerball entfernt
