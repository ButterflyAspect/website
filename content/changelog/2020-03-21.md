---
title: 2020-03-21
date: 2020-03-21
tags:
  - Grundregelwerk
  - Magieregelwerk
categories:
  - Changelog
author: Tealk
summary: Ein Regelwerk update wurde veröffentlicht und folgendes wurde geändert
showToc: false
hidemeta: true
disableHLJS: true
ShowReadingTime: false
ShowShareButtons: true
draft: false
---

## Grundregelwerk

### Changed

- Werte der Rüstungen

## Magieregelwerk

### Changed

- Berechnung der Schwierigkeit
- Runen beschreibung
