---
title: 2021-03-28
date: 2021-03-28
tags:
  - Grundregelwerk
  - Magieregelwerk
categories:
  - Changelog
author: Tealk
summary: Ein Regelwerk update wurde veröffentlicht und folgendes wurde geändert
showToc: false
hidemeta: true
disableHLJS: true
ShowReadingTime: false
ShowShareButtons: true
draft: false
---

## Grundregelwerk

### Add

- Schilde eingefügt

### Changed

- Materialname Eisen bei den Gegeenstandsverbesserungen

### Removed

- Feste Werte in den Fertigkeisbeschreibungen

## Magieregelwerk

### Changed

- Erstellung magische Gegenstände
