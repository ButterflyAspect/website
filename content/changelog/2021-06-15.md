---
title: 2021-06-15
date: 2021-06-15
tags:
  - Grundregelwerk
  - Magieregelwerk
  - Preisliste
categories:
  - Changelog
author: Tealk
summary: Ein Regelwerk update wurde veröffentlicht und folgendes wurde geändert
showToc: false
hidemeta: true
disableHLJS: true
ShowReadingTime: false
ShowShareButtons: true
draft: false
---

## Grundregelwerk

### Add

- Zeiteinteilung für den Tag eingefügt
- Eigenes Kapitel für Kämpfe
- Tabelle für Begleiter eingefügt
- Übersicht der Werte für die Charaktererschaffung
- Fallschaden

### Changed

- Erklärungen der Heilung angepasst
- Erklärungen des Kampfes angepasst
- Aspekte erweitert

## Magieregelwerk

### Changed

- Werte an das [neue Würfelsystem](https://butterfly-aspect.de/changelog/2021-04-08/) angepasst
- Wort und Silbenanzahl genauer erklärt
- Zeit und Entfernung verringert
- Wissen genauer erklärt
- Entfernung und Ziehlbereich als variables Wort getauscht

## Preisliste

### Changed

- Werte an das [neue Würfelsystem](https://butterfly-aspect.de/changelog/2021-04-08/) angepasst
