---
title: Impresssum
date: 2019-08-30
author: "Tealk"
showToc: false
disableHLJS: true
ShowShareButtons: false
---

### Diensteanbieter:

Daniel (Tealk) Buck  
<span style="font-size: 10px;">Herzog von Meranien</span>  
Kirchenstr. 2  
92693 Eslarn

### Kontaktmöglichkeiten:

Telefon: +49 156 78563777  
Telefax: +49 32 1226924-33  
E-Mail: [webmaster@anzahcraft.de](mailto:webmaster@anzahcraft.de)  
Sofern möglich, E-Mails nur Verschlüsselt an uns senden: [OpenPGP (0xBAE7E85C445DB0AE)](https://keys.openpgp.org/vks/v1/by-fingerprint/972933C0E69DA946327490F5BAE7E85C445DB0AE)

## Disclaimer – Haftungs- und Urheberrechtshinweise

### Widerspruch Werbe-Mails

Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-E-Mails, vor.

### Hinweise auf Rechtsverstöße

Sollten Sie innerhalb unseres Internetauftritts Rechtsverstöße bemerken, bitten wir Sie uns auf diese hinzuweisen. Wir werden rechtswidrige Inhalte und Links nach Kenntnisnahme unverzüglich entfernen.
