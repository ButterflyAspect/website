---
title: Lizenz
date: 2019-08-30
author: "Tealk"
showToc: false
disableHLJS: true
ShowShareButtons: false
---

Butterfly Aspect ist ein freies Rollenspiel im Sinne freier Software. Das heißt, es bietet und sichert die **vier grundlegenden Freiheiten**:

- Die Freiheit das Werk zu jedem Zweck zu **verwenden** (Freiheit 0).
- Die Freiheit das Werk an deine Bedürfnisse **anzupassen** (Freiheit 1).
- Die Freiheit Kopien des Werkes **weiterzugeben**, um Nachbarn und Freunden (und deiner Rollenspielrunde!) helfen zu können (Freiheit 2).
- Die Freiheit das Werk zu verbessern und deine **Verbesserungen zu veröffentlichen**, so dass die gesamte Gemeinschaft davon profitiert (Freiheit 3).

Um das Ziel zu erreichen, nutzt es die Creative Commons License

Für Dich heißt das:  
Du kannst mit den Regeln machen was du willst, solange du deine Änderungen und Erweiterungen an den Regeln wieder unter die gleiche Lizenz stellst.

Wenn du unsere Werke nutzt um etwas eigenes zu erschaffen, reicht es meistens, einfach eine Fußzeile wie die folgende zu nutzen:

Butterfly-Aspect © 2021 by Tealk is licensed under CC BY-NC-SA 4.0. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/

Das nennt sich dann "symmetrische Rechte" - du hast alle Rechte, die auch jeder andere Nutzer der Regeln hat, und im Gegenzug gibt du jedem anderen die gleichen Rechte, die du für deine Änderungen hast, und auf diese Art kann sich eine Umgebung formen, in der viele Autoren gleichberechtigt an einer gemeinsamen Grundlage arbeiten, so dass alle deutlich mehr davon hat, als wenn sie alleine arbeiten würden. Durch diese gesichert gleichberechtigte Weitergabe wird kulturelle Freiheit wehrhaft.</div>

Unsere bisher genutzten Programme:

- [Visual Studio Code](https://code.visualstudio.com) für ~~die Regelwerke und~~ die Webseite.
- [texstudio](https://www.texstudio.org/) für die Regelwerke.
- [Gimp](http://www.gimp.org) für Grafiken.
- ~~[GitKraken](https://www.gitkraken.com/git-client) als Versionsverwaltung.~~
- [Fork](https://fork.dev/) als Versionsverwaltung.
