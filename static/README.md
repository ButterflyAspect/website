ButterflyAspect Homepage
============
![Framework](https://img.shields.io/badge/Framework-hugo-pink) ![Lizenz](https://img.shields.io/badge/Lizenz-CC--BY--NC--SA--4.0-orange) [![SourceCode](https://img.shields.io/badge/SoruceCode-codeberg.org-green)](https://codeberg.org/ButterflyAspect/page-source)

# Allgemein
Dies ist das Repository meines d10 Pen and Paper Regelwerks welche mit Hugo erstellt wurde.

## Bilder
Die Hintergrundbilder sind von folgenden Künstlern erstellt worden:
* https://www.artstation.com/aaronflorento
* https://www.artstation.com/jobpmenting