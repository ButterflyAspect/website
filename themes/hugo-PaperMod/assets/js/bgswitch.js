var images = [
  "Boss_Battle_by_Aaron_Florento",
  "Jormundra_Carcass_by_Aaron_Florento",
  "Plain_Fields_by_Job_Menting",
  "Practice_by_Job_Menting",
  "Ravine_Stronghold_by_Aaron_Florento",
  "aaron-florento-graveyardchapel-keyshot-09242018-1944x1037-02",
  "ac2",
  "job-menting-1",
  "job-menting-2",
  "job-menting-artbook-fishmarket-1-2",
  "job-menting-artbook-fishmarket-artstation2",
  "job-menting-challenge1",
  "job-menting-challenge2",
  "job-menting-cuberpunk5finished2",
  "job-menting-cyberpunk1",
  "job-menting-final",
  "job-menting-final1-daylight",
  "job-menting-final3",
  "job-menting-finalartstation",
  "job-menting-finalartstation2",
  "job-menting-finalclub3artstation",
  "job-menting-finishedproduct",
  "job-menting-klaar",
  "job-menting-mountainfinishedkleiner",
  "job-menting-planet3final",
  "job-menting-yiihuu-cyberpunk-city-daytime-final",
  "job-menting-yiihuu-cyberpunk-city-nighttime-final",
];

function check_webp_feature(feature, callback) {
  var kTestImages = {
    lossy: "UklGRiIAAABXRUJQVlA4IBYAAAAwAQCdASoBAAEADsD+JaQAA3AAAAAA",
    lossless: "UklGRhoAAABXRUJQVlA4TA0AAAAvAAAAEAcQERGIiP4HAA==",
    alpha: "UklGRkoAAABXRUJQVlA4WAoAAAAQAAAAAAAAAAAAQUxQSAwAAAARBxAR/Q9ERP8DAABWUDggGAAAABQBAJ0BKgEAAQAAAP4AAA3AAP7mtQAAAA==",
    animation: "UklGRlIAAABXRUJQVlA4WAoAAAASAAAAAAAAAAAAQU5JTQYAAAD/////AABBTk1GJgAAAAAAAAAAAAAAAAAAAGQAAABWUDhMDQAAAC8AAAAQBxAREYiI/gcA",
  };
  var img = new Image();
  img.onload = function () {
    var result = img.width > 0 && img.height > 0;
    callback(feature, result);
  };
  img.onerror = function () {
    callback(feature, false);
  };
  img.src = "data:image/webp;base64," + kTestImages[feature];
}

if (!sessionStorage.getItem("bg-image")) {
  sessionStorage.setItem("bg-image", "/assets/images/bg/" + images[Math.floor(Math.random() * images.length)]);
}

if (sessionStorage.getItem("bg-image")) {
  check_webp_feature("lossy", function (feature, isSupported) {
    if (isSupported) {
      document.body.style.backgroundImage = "url(" + sessionStorage.getItem("bg-image") + ".webp)";
    } else {
      document.body.style.backgroundImage = "url(" + sessionStorage.getItem("bg-image") + ".jpg)";
    }
  });
} else {
  document.body.style.backgroundImage = "url(/assets/images/bg/Boss_Battle_by_Aaron_Florento.jpg)";
}
